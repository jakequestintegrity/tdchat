﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Moq;
using NUnit.Framework;
using TDClient.WinForms.ViewModels;
using TDServer.WinForms;

namespace TDClient.WinForms.Tests
{
    [TestFixture]
    public class ChatControllerTests
    {
        #region Setup/Teardown

        [SetUp]
        public void SetUp()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
        }

        #endregion

        [Test]
        public void BroadcastMessageReceived_NonNullChatMessage_ViewModelAppendsMessage()
        {
            // Arrange
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            var chatViewModel = new ChatViewModel(SynchronizationContext.Current);
            var chatServiceMock = new Mock<IChatService>(MockBehavior.Strict);
            chatServiceMock.Setup(service => service.GetMessages());
            var chatViewMock = new Mock<IChatView>(MockBehavior.Strict);
            chatViewMock.Setup(view => view.BindTo(chatViewModel, loginViewModel));
            var chatController = new ChatController(chatServiceMock.Object,
                chatViewMock.Object, loginViewModel, chatViewModel);

            // Act
            chatServiceMock.Raise(service => service.BroadcastMessageReceived += null,
                new ChatMessageArgs(new ChatMessage {Name = "jake", Message = "Test message"}));

            // Assert
            Assert.That(chatViewModel.MessageList, Does.Contain("jake: Test message"));
        }

        [Test]
        public void BroadcastMessageReceived_NullChatMessage_ThrowsNullReferenceException()
        {
            // Arrange
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            var chatViewModel = new ChatViewModel(SynchronizationContext.Current);
            var chatServiceMock = new Mock<IChatService>(MockBehavior.Strict);
            chatServiceMock.Setup(service => service.GetMessages());
            var chatViewMock = new Mock<IChatView>(MockBehavior.Strict);
            chatViewMock.Setup(view => view.BindTo(chatViewModel, loginViewModel));
            var chatController = new ChatController(chatServiceMock.Object, chatViewMock.Object, loginViewModel,
                chatViewModel);

            // Act + Assert
            Assert.Throws<NullReferenceException>(() =>
                chatServiceMock.Raise(service => service.BroadcastMessageReceived += null, new ChatMessageArgs(null)));
        }

        [Test]
        public void BroadcastMessagesReceived_NonNullChatMessagesList_ViewModelAppendsMessages()
        {
            // Arrange
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            var chatViewModel = new ChatViewModel(SynchronizationContext.Current);
            var chatServiceMock = new Mock<IChatService>(MockBehavior.Strict);
            chatServiceMock.Setup(service => service.GetMessages());
            var chatViewMock = new Mock<IChatView>(MockBehavior.Strict);
            chatViewMock.Setup(view => view.BindTo(chatViewModel, loginViewModel));
            var chatController = new ChatController(chatServiceMock.Object, chatViewMock.Object, loginViewModel,
                chatViewModel);
            var firstMessage = new ChatMessage {Name = "jake", Message = "Hello, is anyone there??"};
            var secondMessage = new ChatMessage {Name = "Bob", Message = "Yep, I'm here..."};

            // Act
            chatServiceMock.Raise(service => service.BroadcastMessagesReceived += null,
                new ChatMessagesArgs(new List<ChatMessage> {firstMessage, secondMessage}));

            // Assert
            Assert.That(chatViewModel.MessageList.First(), Is.EqualTo($"{firstMessage.Name}: {firstMessage.Message}"));
            Assert.That(chatViewModel.MessageList.Last(), Is.EqualTo($"{secondMessage.Name}: {secondMessage.Message}"));
        }

        [Test]
        public void BroadcastMessagesReceived_NullChatMessagesList_ThrowsNullReferenceException()
        {
            // Arrange
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            var chatViewModel = new ChatViewModel(SynchronizationContext.Current);
            var chatServiceMock = new Mock<IChatService>(MockBehavior.Strict);
            chatServiceMock.Setup(service => service.GetMessages());
            var chatViewMock = new Mock<IChatView>(MockBehavior.Strict);
            chatViewMock.Setup(view => view.BindTo(chatViewModel, loginViewModel));
            var chatController = new ChatController(chatServiceMock.Object, chatViewMock.Object, loginViewModel,
                chatViewModel);

            // Act + Assert
            Assert.Throws<ArgumentNullException>(() =>
                chatServiceMock.Raise(service => service.BroadcastMessagesReceived += null,
                    new ChatMessagesArgs(null)));
        }

        [Test]
        public void ChatController_Construction_HasView()
        {
            // Arrange
            var chatServiceMock = new Mock<IChatService>(MockBehavior.Strict);
            var chatViewMock = new Mock<IChatView>(MockBehavior.Strict);
            var chatViewModel = new ChatViewModel(SynchronizationContext.Current);
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            chatServiceMock.SetupAllProperties();
            chatServiceMock.Setup(service => service.GetMessages());
            chatViewMock.Setup(view => view.BindTo(chatViewModel, loginViewModel));

            // Act
            var chatController = new ChatController(chatServiceMock.Object, chatViewMock.Object, loginViewModel,
                chatViewModel);

            // Assert
            Assert.That(chatController.View, Is.Not.Null);
        }

        [Test]
        public void ClientClosing_Raised_ChatServiceDisconnects()
        {
            // Arrange
            var chatServiceMock = new Mock<IChatService>(MockBehavior.Strict);
            var chatViewMock = new Mock<IChatView>(MockBehavior.Strict);
            var chatViewModel = new ChatViewModel(SynchronizationContext.Current);
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            chatServiceMock.SetupAllProperties();
            chatServiceMock.Setup(service => service.Disconnect());
            chatServiceMock.Setup(service => service.GetMessages());
            chatViewMock.Setup(view => view.BindTo(chatViewModel, loginViewModel));
            var chatController = new ChatController(chatServiceMock.Object, chatViewMock.Object, loginViewModel,
                chatViewModel);

            // Act
            chatViewMock.Raise(view => view.ClientClosing += null, EventArgs.Empty);

            // Assert
            chatServiceMock.Verify(service => service.Disconnect(), Times.Once);
        }

        [Test]
        public void ClientConnected_WithConnectionID_UsersNotified()
        {
            // Arrange
            var chatViewModel = new ChatViewModel(SynchronizationContext.Current);
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            var chatServiceMock = new Mock<IChatService>(MockBehavior.Strict);
            chatServiceMock.Setup(service => service.GetMessages());
            var chatViewMock = new Mock<IChatView>(MockBehavior.Strict);
            chatViewMock.Setup(view => view.BindTo(chatViewModel, loginViewModel));
            chatViewMock.Setup(view => view.NotifyClientConnected(It.IsAny<string>()));
            var chatController = new ChatController(chatServiceMock.Object, chatViewMock.Object, loginViewModel,
                chatViewModel);

            // Act
            chatServiceMock.Raise(service => service.ClientConnected += null, new ClientConnectionArgs("ConnectionID"));

            // Assert
            Assert.That(chatViewModel.MessageList.Last(),
                Does.Contain("Client ConnectionID connected (")
                    .And.EndsWith(")"));
            chatViewMock.Verify(view => view.NotifyClientConnected("ConnectionID"), Times.Once);
        }

        [Test]
        public void ClientDisconnected_WithConnectionID_UsersNotified()
        {
            // Arrange
            var chatViewModel = new ChatViewModel(SynchronizationContext.Current);
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            var chatServiceMock = new Mock<IChatService>(MockBehavior.Strict);
            chatServiceMock.Setup(service => service.GetMessages());
            var chatViewMock = new Mock<IChatView>(MockBehavior.Strict);
            chatViewMock.Setup(view => view.BindTo(chatViewModel, loginViewModel));
            chatViewMock.Setup(view => view.NotifyClientConnected(It.IsAny<string>()));
            var chatController = new ChatController(chatServiceMock.Object, chatViewMock.Object, loginViewModel,
                chatViewModel);

            // Act
            chatServiceMock.Raise(service => service.ClientDisconnected += null,
                new ClientConnectionArgs("ConnectionID"));

            // Assert
            var lastMessage = chatViewModel.MessageList.Last();
            Assert.That(lastMessage, Does.Contain("Client ConnectionID disconnected (").And.EndsWith(")"));
            Assert.That(DateTime.Parse(lastMessage.Substring(lastMessage.IndexOf("(") + 1,
                    lastMessage.IndexOf(")") - lastMessage.IndexOf("(") - 1)),
                Is.EqualTo(DateTime.Now).Within(TimeSpan.FromMinutes(1)));
            chatViewMock.Verify(view => view.NotifyClientConnected("ConnectionID"), Times.Never);
        }

        [Test]
        public void SendMessageRequested_Raised_MessageSent()
        {
            // Arrange
            var chatServiceMock = new Mock<IChatService>(MockBehavior.Strict);
            var chatViewMock = new Mock<IChatView>(MockBehavior.Strict);
            var chatViewModel = new ChatViewModel(SynchronizationContext.Current);
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            chatServiceMock.SetupAllProperties();
            chatServiceMock.Setup(service => service.SendMessage(It.IsAny<string>(), It.IsAny<string>()));
            chatServiceMock.Setup(service => service.GetMessages());
            chatViewMock.Setup(view => view.BindTo(chatViewModel, loginViewModel));
            chatViewMock.Setup(view => view.FocusMessageTextBox());
            var chatController = new ChatController(chatServiceMock.Object, chatViewMock.Object, loginViewModel,
                chatViewModel);

            // Act
            chatViewMock.Raise(view => view.SendMessageRequested += null, EventArgs.Empty);

            // Assert
            chatServiceMock.Verify(service => service.SendMessage(loginViewModel.UserName, chatViewModel.Message));
            Assert.That(chatViewModel.Message, Is.Empty);
            chatViewMock.Verify(view => view.FocusMessageTextBox(), Times.Once);
        }

        [Test]
        public void ServerCommandResponse_ResponseReceived_ResponseAppendedToMessages()
        {
            // Arrange
            var chatViewModel = new ChatViewModel(SynchronizationContext.Current);
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            var chatServiceMock = new Mock<IChatService>(MockBehavior.Strict);
            chatServiceMock.Setup(service => service.GetMessages());
            var chatViewMock = new Mock<IChatView>(MockBehavior.Strict);
            chatViewMock.Setup(view => view.BindTo(chatViewModel, loginViewModel));
            var chatController = new ChatController(chatServiceMock.Object, chatViewMock.Object, loginViewModel,
                chatViewModel);

            // Act
            chatServiceMock.Raise(service => service.ServerCommandResponse += null,
                new ServerCommandResponseArgs("I'm doing alright...for a server..."));

            // Assert
            Assert.That(chatViewModel.MessageList.Last(), Is.EqualTo("I'm doing alright...for a server..."));
        }
    }
}