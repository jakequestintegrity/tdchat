﻿using System.Threading.Tasks;
using NUnit.Framework;

namespace TDClient.WinForms.Tests
{
    /// <summary>
    ///     This one doesn't have 100% test coverage.  I stopped where I did because the other
    ///     aspects of the class involve verifying that behavior supposedly provided by a third
    ///     party is there responsibility.  Yes, you can pick a library that doesn't do a very
    ///     good job convincing you that their shit works.  Yes, their code might not be the
    ///     best code ever written.  But if we spend the time to write perfect code for everything
    ///     we provide, we'll never make a deadline.  Pick 3rd party libraries that provide value
    ///     in a verifiable way that is currently being maintained and can be perceived to follow
    ///     best practices.  We might be able to write something better, but maybe we should do
    ///     that only when we absolutely have to.  Integrating with 3rd party libraries is really
    ///     just a form of staff augmentation that can come from a much larger, more capable team,
    ///     all things considered.
    /// </summary>
    [TestFixture]
    internal class ChatServiceTests
    {
        [Test]
        public async Task ConnectionClosed_CallbackProvided_CallbackInvokedOnDisconnect()
        {
            // Arrange
            var ranConnectionClosedCallback = false;
            var chatService = new ChatService();
            chatService.ConnectionClosed += (sender, args) => ranConnectionClosedCallback = true;
            
            // Act
            await chatService.ConnectAsync();
            chatService.Disconnect();

            // Assert
            Assert.That(ranConnectionClosedCallback, Is.True);
        }
    }
}