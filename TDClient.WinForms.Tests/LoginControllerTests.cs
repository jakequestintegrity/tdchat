﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using TDClient.WinForms.ViewModels;

namespace TDClient.WinForms.Tests
{
    [TestFixture]
    public class LoginControllerTests
    {
        [SetUp]
        public void SetUp()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
        }

        [Test]
        public void AuthenticationRequested_RaisedWithInvalidUsername_NotSignedIn()
        {
            // Arrange
            var successfullyLoggedIn = false;
            var chatServiceMock = new Mock<IChatService>(MockBehavior.Strict);
            var loginViewMock = new Mock<ILoginView>(MockBehavior.Strict);
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            chatServiceMock.SetupAllProperties();
            chatServiceMock.SetupGet(service => service.ServerURI).Returns("http://localhost:8080");
            chatServiceMock.Setup(service => service.GetMessages());
            loginViewMock.Setup(view => view.BindTo(loginViewModel));
            var loginController = new LoginController(loginViewMock.Object, chatServiceMock.Object, loginViewModel)
            {
                SuccessfullyLoggedIn = loggedInSuccessfully => successfullyLoggedIn = loggedInSuccessfully
            };
            loginViewModel.UserName = string.Empty;

            // Act
            loginViewMock.Raise(view => view.AuthenticationRequested += null, EventArgs.Empty);

            // Assert
            chatServiceMock.Verify(service => service.ConnectAsync(), Times.Never());
            Assert.That(successfullyLoggedIn, Is.False);
        }

        [Test]
        public void AuthenticationRequested_RaisedWithValidUserName_SignedIn()
        {
            // Arrange
            var successfullyLoggedIn = false;
            var chatServiceMock = new Mock<IChatService>(MockBehavior.Strict);
            var loginViewMock = new Mock<ILoginView>(MockBehavior.Strict);
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            chatServiceMock.SetupAllProperties();
            chatServiceMock.Setup(service => service.ConnectAsync()).Returns(Task.FromResult(true));
            chatServiceMock.SetupGet(service => service.ServerURI).Returns("http://localhost:8080");
            chatServiceMock.Setup(service => service.GetMessages());
            loginViewMock.Setup(view => view.BindTo(loginViewModel));
            var loginController = new LoginController(loginViewMock.Object, chatServiceMock.Object, loginViewModel)
            {
                SuccessfullyLoggedIn = loggedInSuccessfully => successfullyLoggedIn = loggedInSuccessfully
            };
            loginViewModel.UserName = "jake";

            // Act
            loginViewMock.Raise(view => view.AuthenticationRequested += null, EventArgs.Empty);

            // Assert
            chatServiceMock.Verify(service => service.ConnectAsync(), Times.Once);
            Assert.That(successfullyLoggedIn, Is.True);
        }
        
        [Test]
        public void Constructor_NonNullLoginVM_StatusSetToPleaseLogin()
        {
            // Arrange
            var loginViewModel = new LoginViewModel(SynchronizationContext.Current);
            var loginViewMock = new Mock<ILoginView>(MockBehavior.Strict);
            loginViewMock.Setup(view => view.BindTo(loginViewModel));

            // Act
            var loginController = new LoginController(loginViewMock.Object,
                new Mock<IChatService>(MockBehavior.Strict).Object, loginViewModel);

            // Assert
            Assert.That(loginViewModel.Status, Is.EqualTo("Please login."));
        }

        [Test]
        public void Constructor_NullLoginVM_ThrowsNullReferenceException()
        {
            // Arrange
            LoginViewModel loginViewModel = null;

            // Act + Assert
            Assert.Throws<NullReferenceException>(() => new LoginController(
                new Mock<ILoginView>(MockBehavior.Strict).Object,
                new Mock<IChatService>(MockBehavior.Strict).Object, loginViewModel));
        }
    }
}