﻿using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;

namespace TDClient.WinForms.Tests
{
    /*
     * Name tests in the following manner to keep a consistent naming convention
     * in place that will help any current and future developers aware of the
     * expected behavior of the system they are tasked with working on.
     * 
     * [UnitOfWork_StateUnderTest_ExpectedBehavior]
     * -- or --
     * [MethodName_StateUnderTest_ExpectedBehavior]
     * (Test name should include nameof tested method or class)
     * 
     * Roy Osherove (Team Leadership, Agile & Lean Development, Ruby .NET & 
     * Java-Speaking, Consulting, Trainging and Tools)
     * 
     * Test name should express a specific requirement
     * 
     * Test name should include the expected input or state and the exptected result
     * for that input or state.
     * 
     * Test name should be presented as a statement or fact of life that expresses
     * workflows and outputs
     * http://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html
     */
    [TestFixture]
    internal class ChatViewModelTests
    {
        [SetUp]
        public void SetUp()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
        }

        [Test]
        public void AppendMessage_ArbitraryString_MessagesContainsArbitraryString()
        {
            // Arrange
            var vm = new ChatViewModel(SynchronizationContext.Current);

            // Act
            vm.AppendMessage("First message");

            // Assert
            Assert.That(vm.Messages, Does.Contain("First message"));
        }

        [Test]
        public void AppendMessage_Twice_MessagesAreInCorrectOrder()
        {
            // Arrange
            var vm = new ChatViewModel(SynchronizationContext.Current);

            // Act
            vm.AppendMessage("First message");
            vm.AppendMessage("Second message");

            // Assert
            Assert.That(vm.Messages.IndexOf("First", StringComparison.Ordinal),
                Is.LessThan(vm.Messages.IndexOf("Second", StringComparison.Ordinal)));
        }

        [Test]
        public void AppendMessage_Twice_MessagesAreOnDifferentLines()
        {
            // Arrange
            var vm = new ChatViewModel(SynchronizationContext.Current);

            // Act
            vm.AppendMessage("First message");
            vm.AppendMessage("Second message");

            // Assert
            var messagesSplit = vm.Messages.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);
            Assert.That(messagesSplit.First(), Is.EqualTo("First message"));
            Assert.That(messagesSplit.Last(), Is.EqualTo("Second message"));
        }
    }
}