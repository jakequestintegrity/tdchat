﻿using System.Collections.Generic;

namespace TDServer.WinForms
{
    /// <summary>
    /// Static HashSet to keep track of connected ids to the hub.
    /// 
    /// <remarks>This assumes one server.  This is inadequate for server sharding.</remarks>
    /// </summary>
    public static class UserHandler
    {
        public static HashSet<string> ConnectedIds = new HashSet<string>();
    }
}