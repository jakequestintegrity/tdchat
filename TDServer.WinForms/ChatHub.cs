﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace TDServer.WinForms
{
    /// <inheritdoc />
    /// <summary>
    ///     Echoes messages sent using the Send message by calling the broadcastMessage method on the client.
    ///     Also reports to the console when clients connect and disconnect.
    /// </summary>
    public class ChatHub : Hub
    {
        private readonly IChatRepository _repository;

        /// <summary>
        /// Suppose that you want to store chat messages on the server before sending them.
        /// You might define an interface that abstracts this functionality, and use DI to
        /// inject the interface into the ChatHub class.
        /// 
        /// The only problem is that a SignalR application does not directly create hubs;
        /// SignalR creates them for you.  By default, SignalR expects a hub class to have
        /// a parameterless constructor.  However, you can easily register a function to
        /// create hub instances, and use this function to perform DI.  Register the function
        /// by calling GlobalHost.DependencyResolver.Register in the Configuration method of
        /// your Startup class.
        /// </summary>
        /// <param name="repository"></param>
        public ChatHub(IChatRepository repository)
        {
            _repository = repository;
        }
        
        public override Task OnConnected()
        {
            Clients.All.clientConnected(Context.ConnectionId);
            UserHandler.ConnectedIds.Add(Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Clients.All.clientDisconnected(Context.ConnectionId);
            UserHandler.ConnectedIds.Remove(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            Clients.Caller.reconnected();
            return base.OnReconnected();
        }

        public void Send(string name, string message)
        {
            if (message == "server status")
            {
                message =
                    $"Server: I'm doing fine.  Thanks for asking.  I have {UserHandler.ConnectedIds.Count} Clients connected at the moment.";
                Clients.Caller.serverCommandResponse(message);
            }
            else
            {
                var chatMessage = _repository.Add(name, message);
                Clients.All.broadcastMessage(chatMessage);
            }
        }

        public void GetMessages()
        {
            var chatMessages = _repository.GetMessages();
            Clients.Caller.broadcastMessages(chatMessages);
        }
    }
}