using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDServer.WinForms
{
    [Table("ChatMessages")]
    public class ChatMessage
    {
        public override string ToString() => $"{Name.Trim()}: {Message.Trim()}";

        #region Properties, Indexers

        [Required]
        [StringLength(10)]
        public string Name { get; set; }

        [Required]
        public string Message { get; set; }

        [Key]
        public int MessageId { get; set; }

        public DateTime? CreatedDate { get; set; }

        #endregion
    }
}