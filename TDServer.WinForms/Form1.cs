﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using Microsoft.Owin.Hosting;

namespace TDServer.WinForms
{
    public partial class ServerWindow : MetroForm
    {
        #region Static Fields and Constants

        private const string ServerURI = "http://localhost:8080";

        #endregion

        #region Constructors

        public ServerWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Properties, Indexers

        public IDisposable SignalR { get; set; }

        #endregion

        /// <summary>
        ///     Calls the StartServer method with Task.Run to nto block the UI thread.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startButton_Click(object sender, EventArgs e)
        {
            WriteToConsole("Starting server...");
            startButton.Enabled = false;
            Task.Run(() => StartServer());
        }

        /// <summary>
        ///     Starts the server and checks for error thrown when another server is already
        ///     running.  This method is called asynchronously from startButton.
        /// </summary>
        private void StartServer()
        {
            try
            {
                SignalR = WebApp.Start(ServerURI);
            }
            catch (TargetInvocationException)
            {
                WriteToConsole("Server failed to start.  A server is already running on " + ServerURI);
                // Re-enable button to let user try to start server again.
                Invoke((Action) (() => startButton.Enabled = true));
                return;
            }
            Invoke((Action) (() => startButton.Enabled = true));
            WriteToConsole("Server started at " + ServerURI);
        }

        /// <summary>
        ///     Stops the server and closes the form.  Restart functionality omitted for clarity.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void stopButton_Click(object sender, EventArgs e)
        {
            // SignalR will be disposed in the FormClosing event
            Close();
        }

        private void WriteToConsole(string message)
        {
            if (multilineTextBox.InvokeRequired)
            {
                Invoke((Action) (() => WriteToConsole(message)));
                return;
            }
            multilineTextBox.AppendText(message + Environment.NewLine);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) => SignalR?.Dispose();
    }
}