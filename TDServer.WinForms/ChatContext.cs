﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace TDServer.WinForms
{
    internal class ChatContext : DbContext
    {
        #region Properties, Indexers

        public DbSet<ChatMessage> ChatMessages { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChatMessage>()
                .ToTable("ChatMessages");
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}