﻿using System.Collections.Generic;
using System.Linq;

namespace TDServer.WinForms
{
    public interface IChatRepository
    {
        ChatMessage Add(string name, string message);

        List<ChatMessage> GetMessages();
    }
}