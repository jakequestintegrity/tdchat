﻿using System.Collections.Generic;
using System.Linq;

namespace TDServer.WinForms
{
    public class ChatMessageRepository : IChatRepository
    {
        public ChatMessage Add(string name, string message)
        {
            var chatMessage = new ChatMessage { Name = name, Message = message };
            int changes;
            using (var db = new ChatContext())
            {
                db.ChatMessages.Add(chatMessage);
                changes = db.SaveChanges();
            }
            return changes == 1 ? chatMessage : null;
        }

        public List<ChatMessage> GetMessages()
        {
            //return new ChatMessage[]{};
            using (var db = new ChatContext())
            {
                return db.ChatMessages.ToList();
            }
        }
    }
}