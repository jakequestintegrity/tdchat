namespace TDServer.WinForms
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Message
    {
        [Required]
        [StringLength(10)]
        public string Name { get; set; }

        [Column("Message")]
        [Required]
        public string Message1 { get; set; }

        public int MessageId { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}
