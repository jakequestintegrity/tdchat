﻿using System;
using TDClient.WinForms.ViewModels;

namespace TDClient.WinForms
{
    public class LoginController
    {
        #region Fields

        private readonly IChatService _chatService;
        private readonly LoginViewModel _loginVM;

        #endregion

        #region Constructors

        public LoginController(ILoginView view, IChatService chatService, LoginViewModel loginVM)
        {
            _chatService = chatService;
            _loginVM = loginVM;
            _loginVM.Status = "Please login.";
            View = view;
            View.AuthenticationRequested += ViewOnAuthenticationRequested;
            View.BindTo(_loginVM);
        }

        #endregion

        #region Properties, Indexers

        public ILoginView View { get; }
        public Action<bool> SuccessfullyLoggedIn { get; set; }

        #endregion

        private void ViewOnAuthenticationRequested(object sender, EventArgs eventArgs) => SignInAsync();

        private async void SignInAsync()
        {
            // Connect to server (use async method to avoid blocking UI thread)
            if (string.IsNullOrEmpty(_loginVM.UserName)) return;
            _loginVM.Status = "Connecting to server...";
            _loginVM.IsNotLoading = false;
            var response = await _chatService.ConnectAsync();
            SuccessfullyLoggedIn?.Invoke(response);
        }
    }
}