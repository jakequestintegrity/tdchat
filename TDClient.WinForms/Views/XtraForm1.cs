﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Skins;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraBars.ToastNotifications;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using TDClient.WinForms.ViewModels;

namespace TDClient.WinForms
{
    public partial class XtraForm1 : XtraForm, IChatView
    {
        #region Fields

        private string _userName = string.Empty;

        #endregion

        #region Constructors

        public XtraForm1()
        {
            InitializeComponent();
            var skin = CommonSkins.GetSkin(defaultLookAndFeel1.LookAndFeel);
            var color = skin.TranslateColor(SystemColors.Control);
            tileBar.BackColor = color;
            navigationFrame.BackColor = color;
        }

        #endregion

        #region Interface Implementations

        public event EventHandler SendMessageRequested;

        public void FocusMessageTextBox()
        {
            //while (textEdit1.Focused != true) textEdit1.Focus();
            textEdit1.Focus();
        }

        public event EventHandler ClientClosing;

        public void BindTo(ChatViewModel chatVM, LoginViewModel loginVM)
        {
            _userName = loginVM.UserName;
            textEdit1.DataBindings.Add(nameof(textEdit1.Text), chatVM, nameof(chatVM.Message), false,
                DataSourceUpdateMode.OnPropertyChanged);
            gridControl1.DataSource = chatVM.MessageList;
        }

        public void NotifyClientConnected(string connectionID)
        {
            toastNotificationsManager1.ShowNotification(new ToastNotification("object_id", null, "Client Connected",
                $"{connectionID} connected ({DateTime.Now:G})", "FART", ToastNotificationSound.IM,
                ToastNotificationDuration.Default, ToastNotificationTemplate.Text02));
        }

        #endregion

        private void tileBar_SelectedItemChanged(object sender, TileItemEventArgs e)
        {
            navigationFrame.SelectedPageIndex = tileBarGroupTables.Items.IndexOf(e.Item);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            var message = (string) gridView1.GetRow(e.RowHandle);
            e.Appearance.TextOptions.HAlignment = message.Split(':')
                                                      .First() == _userName
                ? HorzAlignment.Far
                : HorzAlignment.Near;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            SendMessageRequested?.Invoke(this, EventArgs.Empty);
        }

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char) 13) return;
            SendMessageRequested?.Invoke(this, EventArgs.Empty);
            e.Handled = true;
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            // TODO: Handle logout
        }

        private void XtraForm1_FormClosing(object sender, FormClosingEventArgs e) =>
            ClientClosing?.Invoke(this, EventArgs.Empty);
    }
}