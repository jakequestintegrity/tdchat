﻿using System;
using TDClient.WinForms.ViewModels;

namespace TDClient.WinForms
{
    public interface IChatView
    {
        event EventHandler SendMessageRequested;
        void FocusMessageTextBox();
        event EventHandler ClientClosing;
        void BindTo(ChatViewModel chatVM, LoginViewModel loginVM);
        void NotifyClientConnected(string connectionID);
    }
}