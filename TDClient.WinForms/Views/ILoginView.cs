﻿using System;
using TDClient.WinForms.ViewModels;

namespace TDClient.WinForms
{
    public interface ILoginView
    {
        event EventHandler AuthenticationRequested;
        void BindTo(LoginViewModel vm);
    }
}