﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using TDClient.WinForms.ViewModels;

namespace TDClient.WinForms
{
    public partial class SplashScreen1 : SplashScreen, ILoginView
    {
        public enum SplashScreenCommand
        {
        }

        #region Constructors

        public SplashScreen1()
        {
            InitializeComponent();
        }

        #endregion

        #region Interface Implementations

        public event EventHandler AuthenticationRequested;

        public void BindTo(LoginViewModel vm)
        {
            textEdit1.DataBindings.Add(nameof(textEdit1.EditValue), vm, nameof(vm.UserName), false,
                DataSourceUpdateMode.OnPropertyChanged);
            marqueeProgressBarControl1.DataBindings.Add("Stopped", vm, nameof(vm.IsNotLoading), false,
                DataSourceUpdateMode.OnPropertyChanged);
            statusLabel.DataBindings.Add(nameof(statusLabel.Text), vm, nameof(vm.Status), false,
                DataSourceUpdateMode.OnPropertyChanged);
        }

        #endregion

        #region Overrides

        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        #endregion

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            AuthenticationRequested?.Invoke(this, EventArgs.Empty);
        }

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char) 13) return;
            AuthenticationRequested?.Invoke(this, EventArgs.Empty);
        }
    }

    public class TDMarqueeProgressBarControl : MarqueeProgressBarControl
    {
        #region Properties, Indexers

        public bool Stopped
        {
            get => Properties.Stopped;
            set => Properties.Stopped = value;
        }

        #endregion
    }
}