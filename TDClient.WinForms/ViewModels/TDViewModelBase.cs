﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using TDClient.WinForms.Annotations;

namespace TDClient.WinForms.ViewModels
{
    public class TDViewModelBase : INotifyPropertyChanged
    {
        #region Fields

        protected readonly SynchronizationContext Context;

        #endregion

        #region Constructors

        public TDViewModelBase(SynchronizationContext context)
        {
            Context = context;
        }

        #endregion

        #region Interface Implementations

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) =>
            Context.Post(o =>
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName)), null);
    }
}