﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using DevExpress.DataProcessing;
using TDClient.WinForms.ViewModels;

namespace TDClient.WinForms
{
    public class ChatViewModel : TDViewModelBase
    {
        #region Fields

        private string _message = string.Empty;
        private BindingList<string> _messageList = new BindingList<string>();
        private string _messages = string.Empty;

        #endregion

        #region Constructors

        public ChatViewModel(SynchronizationContext context) : base(context)
        {
            _messageList.ListChanged += (sender, args) => OnPropertyChanged(nameof(MessageList));
        }

        #endregion

        #region Properties, Indexers

        public string Message
        {
            get => _message;
            set
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }

        public string Messages
        {
            get => _messages;
            set
            {
                _messages = value;
                OnPropertyChanged(nameof(Messages));
            }
        }

        public BindingList<string> MessageList
        {
            get => _messageList;
            set
            {
                _messageList = value;
                OnPropertyChanged(nameof(MessageList));
            }
        }

        #endregion

        public void SetMessages(List<string> messages)
        {
            Context.Send(o =>
            {
                MessageList.Clear();
                MessageList.AddRange(messages);
            }, null);
        }

        public void AppendMessage(string message)
        {
            Messages = new StringBuilder(_messages).AppendLine(message)
                .ToString();
            Context.Send(o => MessageList.Add(message), null);
        }

        internal void AppendMessage(string name, string message)
        {
            Messages = new StringBuilder(_messages).AppendLine($"{name}: {message}")
                .ToString();
            Context.Send(o => MessageList.Add($"{name}: {message}"), null);
        }
    }
}