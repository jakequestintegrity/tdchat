﻿using System.Threading;

namespace TDClient.WinForms.ViewModels
{
    public class LoginViewModel : TDViewModelBase
    {
        #region Fields

        private bool _isNotLoading = true;

        private string _userName = string.Empty;

        #endregion

        #region Constructors

        public LoginViewModel(SynchronizationContext context) : base(context)
        {
        }

        #endregion

        #region Properties, Indexers

        public bool IsNotLoading
        {
            get => _isNotLoading;
            set
            {
                _isNotLoading = value;
                OnPropertyChanged(nameof(IsNotLoading));
            }
        }

        /// <summary>
        ///     This name is simply added to sent messages to identify the user; this
        ///     sample does not include authentication.
        /// </summary>

        public string UserName
        {
            get => _userName;
            set
            {
                _userName = value;
                OnPropertyChanged(nameof(UserName));
            }
        }

        public string Status { get; set; }

        #endregion
    }
}