﻿using System;
using System.Linq;
using TDClient.WinForms.ViewModels;

namespace TDClient.WinForms
{
    public class ChatController
    {
        #region Fields

        private readonly IChatService _chatService;
        private readonly ChatViewModel _chatVM;
        private readonly LoginViewModel _loginVM;

        #endregion

        #region Constructors

        public ChatController(IChatService chatService, IChatView view, LoginViewModel loginVM, ChatViewModel chatVm)
        {
            _chatService = chatService;
            _chatVM = chatVm;
            _loginVM = loginVM;
            View = view;
            _chatService.ConnectionClosed += (sender, args) =>
            {
                // TODO: Notification?
            };
            _chatService.BroadcastMessageReceived += OnBroadcastMessageReceived;
            _chatService.BroadcastMessagesReceived += OnBroadcastMessagesReceived;
            _chatService.ServerCommandResponse += OnServerCommandResponse;
            _chatService.ClientConnected += OnClientConnected;
            _chatService.ClientDisconnected += OnClientDisconnected;
            _chatService.GetMessages();
            View.BindTo(_chatVM, _loginVM);
            View.SendMessageRequested += ViewOnSendMessageRequested;
            View.ClientClosing += ViewOnClientClosing;
        }

        #endregion

        #region Properties, Indexers

        public IChatView View { get; }

        #endregion

        #region Private Methods

        private static string ClientConnectedMessage(string connectionID) =>
            $"Client {connectionID} connected ({DateTime.Now:G})";

        private static string ClientDisconnectedMessage(string connectionID) =>
            $"Client {connectionID} disconnected ({DateTime.Now:G})";

        private void OnBroadcastMessageReceived(object sender, ChatMessageArgs args) =>
            _chatVM.AppendMessage(args.ChatMessage.ToString());

        private void OnBroadcastMessagesReceived(object sender, ChatMessagesArgs args) =>
            _chatVM.SetMessages(args.ChatMessages.Select(m => m.ToString()).ToList());

        private void OnClientConnected(object sender, ClientConnectionArgs args)
        {
            _chatVM.AppendMessage(ClientConnectedMessage(args.Message));
            View.NotifyClientConnected(args.Message);
        }

        private void OnClientDisconnected(object sender, ClientConnectionArgs args) =>
            _chatVM.AppendMessage(ClientDisconnectedMessage(args.Message));

        private void OnServerCommandResponse(object sender, ServerCommandResponseArgs args) =>
            _chatVM.AppendMessage(args.Response);

        private void SendMessage()
        {
            _chatService.SendMessage(_loginVM.UserName, _chatVM.Message);
            _chatVM.Message = string.Empty;
            View.FocusMessageTextBox();
        }

        private void ViewOnClientClosing(object sender, EventArgs eventArgs) => _chatService.Disconnect();

        private void ViewOnSendMessageRequested(object sender, EventArgs eventArgs) => SendMessage();

        #endregion
    }
}