﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.UserSkins;
using TDClient.WinForms.ViewModels;

namespace TDClient.WinForms
{
    internal static class Program
    {
        #region Static Fields and Constants

        private static readonly ApplicationContext MainContext = new ApplicationContext();

        #endregion

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            BonusSkins.Register();
            SkinManager.EnableFormSkins();
            UserLookAndFeel.Default.SetStyle(LookAndFeelStyle.Skin, false, false, "Metropolis Dark");
            //Application.Run((Form) new ChatController(new ChatService(), new ChatForm(),
            //    new ChatViewModel(SynchronizationContext.Current)).View);
            //Application.Run(new XtraForm1());

            var loginView = new SplashScreen1();
            var context = SynchronizationContext.Current;
            var loginViewModel = new LoginViewModel(context);
            var chatService = new ChatService();
            var loginController = new LoginController(loginView, chatService, loginViewModel);
            MainContext.MainForm = (Form) loginController.View;
            loginController.SuccessfullyLoggedIn = response =>
            {
                if (!response) return;
                var loginForm = MainContext.MainForm;
                var chatController = new ChatController(chatService, new XtraForm1(), loginViewModel,
                    new ChatViewModel(context));
                MainContext.MainForm = (Form) chatController.View;
                MainContext.MainForm.Show();
                loginForm.Close();
            };
            Application.Run(MainContext);
        }
    }
}