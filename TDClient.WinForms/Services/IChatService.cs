﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TDServer.WinForms;

namespace TDClient.WinForms
{
    public interface IChatService
    {
        #region Properties, Indexers

        string ServerURI { get; }

        #endregion

        #region Public Methods

        Task<bool> ConnectAsync();
        void Disconnect();
        void GetMessages();

        void SendMessage(string userName, string message);

        #endregion

        event EventHandler ConnectionClosed;
        event EventHandler<ChatMessageArgs> BroadcastMessageReceived;
        event EventHandler<ChatMessagesArgs> BroadcastMessagesReceived;
        event EventHandler<ServerCommandResponseArgs> ServerCommandResponse;
        event EventHandler<ClientConnectionArgs> ClientConnected;
        event EventHandler<ClientConnectionArgs> ClientDisconnected;
    }

    public class ClientConnectionArgs : EventArgs
    {
        #region Constructors

        public ClientConnectionArgs(string message) => Message = message;

        #endregion

        #region Properties, Indexers

        public string Message { get; }

        #endregion
    }

    public class ServerCommandResponseArgs : EventArgs
    {
        #region Constructors

        public ServerCommandResponseArgs(string response) => Response = response;

        #endregion

        #region Properties, Indexers

        public string Response { get; }

        #endregion
    }

    public class ChatMessagesArgs : EventArgs
    {
        #region Constructors

        public ChatMessagesArgs(List<ChatMessage> chatMessages) => ChatMessages = chatMessages;

        #endregion

        #region Properties, Indexers

        public List<ChatMessage> ChatMessages { get; }

        #endregion
    }

    public class ChatMessageArgs : EventArgs
    {
        #region Constructors

        public ChatMessageArgs(ChatMessage chatMessage) => ChatMessage = chatMessage;

        #endregion

        #region Properties, Indexers

        public ChatMessage ChatMessage { get; }

        #endregion
    }
}