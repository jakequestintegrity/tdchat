﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using TDServer.WinForms;

namespace TDClient.WinForms
{
    public class ChatService : IChatService
    {
        #region Static Fields and Constants

        private const string URI = "http://localhost:8080/signalr";

        #endregion

        #region Properties, Indexers

        private IHubProxy HubProxy { get; set; }
        private HubConnection Connection { get; set; }

        #endregion

        #region IChatService Members

        public string ServerURI => URI;

        public event EventHandler ConnectionClosed;
        public event EventHandler<ChatMessageArgs> BroadcastMessageReceived;
        public event EventHandler<ChatMessagesArgs> BroadcastMessagesReceived;
        public event EventHandler<ServerCommandResponseArgs> ServerCommandResponse;
        public event EventHandler<ClientConnectionArgs> ClientConnected;
        public event EventHandler<ClientConnectionArgs> ClientDisconnected;

        #endregion

        #region Private Methods

        private void Connection_Closed()
        {
            ConnectionClosed?.Invoke(this, EventArgs.Empty);
        }

        #endregion

        #region Interface Implementations

        /// <summary>
        ///     Creates and connects the hub connection and hub proxy.
        /// </summary>
        public async Task<bool> ConnectAsync()
        {
            Connection = new HubConnection(ServerURI);
            Connection.Closed += Connection_Closed;
            HubProxy = Connection.CreateHubProxy("ChatHub");
            //Handle incoming event from server: use Invoke to write to console from SignalR's thread
            HubProxy.On<ChatMessage>("BroadcastMessage",
                chatMessage => BroadcastMessageReceived?.Invoke(this, new ChatMessageArgs(chatMessage)));
            HubProxy.On<List<ChatMessage>>("BroadcastMessages",
                messages => BroadcastMessagesReceived?.Invoke(this, new ChatMessagesArgs(messages)));
            HubProxy.On<string>("ServerCommandResponse",
                message => ServerCommandResponse?.Invoke(this, new ServerCommandResponseArgs(message)));
            HubProxy.On<string>("ClientConnected",
                connectionID => ClientConnected?.Invoke(this, new ClientConnectionArgs(connectionID)));
            HubProxy.On<string>("ClientDisconnected",
                connectionID => ClientDisconnected?.Invoke(this, new ClientConnectionArgs(connectionID)));
            try
            {
                await Connection.Start();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Unable to connect to server: Start server before connecting clients.");
                return false;
            }
            return true;
        }

        public void Disconnect()
        {
            if (Connection == null) return;
            Connection.Stop();
            Connection.Dispose();
        }

        public void SendMessage(string userName, string message)
        {
            HubProxy.Invoke("Send", userName, message);
        }

        public void GetMessages()
        {
            HubProxy.Invoke("GetMessages");
        }

        #endregion
    }
}