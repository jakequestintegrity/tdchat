﻿using System.ComponentModel;
using System.Net.Http;
using System.Windows;
using Microsoft.AspNet.SignalR.Client;
using TDServer.WinForms;

namespace TDClient.WPF
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Static Fields and Constants

        private const string ServerURI = "http://localhost:8080/signalr";

        #endregion

        #region Constructors

        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Properties, Indexers

        public string UserName { get; set; }
        public IHubProxy HubProxy { get; set; }
        public HubConnection Connection { get; set; }

        #endregion

        private void SignInButton_OnClick(object sender, RoutedEventArgs e)
        {
            UserName = UserNameTextBox.Text;
            if (string.IsNullOrEmpty(UserName)) return;
            StatusText.Visibility = Visibility.Visible;
            StatusText.Content = "Connecting to server...";
            ConnectAsync();
        }

        private void ButtonSend_OnClick(object sender, RoutedEventArgs e)
        {
            HubProxy.Invoke("Send", UserName, TextBoxMessage.Text);
            TextBoxMessage.Text = string.Empty;
            TextBoxMessage.Focus();
        }

        private async void ConnectAsync()
        {
            Connection = new HubConnection(ServerURI);
            Connection.Closed += Connection_Closed;
            HubProxy = Connection.CreateHubProxy("ChatHub");
            HubProxy.On<ChatMessage>("BroadcastMessage",
                (chatMessage) => Dispatcher.Invoke(() =>
                    RichTextBoxConsole.AppendText($"{chatMessage.Name}: {chatMessage.Message}\r")));
            HubProxy.On<string>("ServerCommandResponse",
                message => Dispatcher.Invoke(() => RichTextBoxConsole.AppendText($"{message}\r")));
            try
            {
                await Connection.Start();
            }
            catch (HttpRequestException)
            {
                StatusText.Content = "Unable to connect to server: Start server before connectting clients.";
                return;
            }
            SignInPanel.Visibility = Visibility.Collapsed;
            ChatPanel.Visibility = Visibility.Visible;
            ButtonSend.IsEnabled = true;
            TextBoxMessage.Focus();
            RichTextBoxConsole.AppendText("Connected to server at " + ServerURI + "\r");
        }

        private void Connection_Closed()
        {
            var dispatcher = Application.Current.Dispatcher;
            dispatcher.Invoke(() => ChatPanel.Visibility = Visibility.Collapsed);
            dispatcher.Invoke(() => ButtonSend.IsEnabled = false);
            dispatcher.Invoke(() => StatusText.Content = "You have been disconnected.");
            dispatcher.Invoke(() => SignInPanel.Visibility = Visibility.Visible);
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            if (Connection == null) return;
            Connection.Stop();
            Connection.Dispose();
        }
    }
}