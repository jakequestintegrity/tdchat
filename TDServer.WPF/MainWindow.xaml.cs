﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Owin;

namespace TDServer.WPF
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Static Fields and Constants

        private const string ServerURI = "http://localhost:8080";

        #endregion

        #region Constructors

        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Properties, Indexers

        public IDisposable SignalR { get; set; }

        #endregion

        private void ButtonStart_OnClick(object sender, RoutedEventArgs e)
        {
            WriteToConsole("Starting server...");
            ButtonStart.IsEnabled = false;
            Task.Run(() => StartServer());
        }

        private void StartServer()
        {
            try
            {
                SignalR = WebApp.Start(ServerURI);
            }
            catch (TargetInvocationException)
            {
                WriteToConsole("A server is already running at " + ServerURI);
                Dispatcher.Invoke(() => ButtonStart.IsEnabled = true);
                return;
            }
            Dispatcher.Invoke(() => ButtonStop.IsEnabled = true);
            WriteToConsole("Server started at " + ServerURI);
        }

        public void WriteToConsole(string message)
        {
            if (!RichTextBoxConsole.CheckAccess())
            {
                Dispatcher.Invoke(() => { WriteToConsole(message); });
                return;
            }
            RichTextBoxConsole.AppendText(message + "\r");
        }

        private void ButtonStop_OnClick(object sender, RoutedEventArgs e)
        {
            SignalR.Dispose();
            Close();
        }
    }

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR();
        }
    }

    public class ChatHub : Hub
    {
        public void Send(string name, string message)
        {
            Clients.All.broadcastMessage(name, message);
        }

        public override Task OnConnected()
        {
            Application.Current.Dispatcher.Invoke(() =>
                ((MainWindow) Application.Current.MainWindow).WriteToConsole(
                    "Client connected: " + Context.ConnectionId));
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Application.Current.Dispatcher.Invoke(() =>
                ((MainWindow) Application.Current.MainWindow).WriteToConsole(
                    "Client disconnected: " + Context.ConnectionId));
            return base.OnDisconnected(stopCalled);
        }
    }
}