﻿using System;
using System.Dynamic;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Moq;
using NUnit.Framework;

namespace TDServer.WinForms.Tests
{
    [TestFixture]
    internal class ChatHubTests
    {
        private Mock<IChatRepository> _chatRepositoryMock;
        private ChatHub _chatHub;
        private Mock<IHubCallerConnectionContext<dynamic>> _mockClients;

        [SetUp]
        public void SetUp()
        {
            _chatRepositoryMock = new Mock<IChatRepository>(MockBehavior.Strict);
            _chatHub = new ChatHub(_chatRepositoryMock.Object)
            {
                Context = new HubCallerContext(new Mock<IRequest>(MockBehavior.Strict).Object, "JakeConnectionID")
            };
            _mockClients = new Mock<IHubCallerConnectionContext<dynamic>>();
            _chatHub.Clients = _mockClients.Object;
        }

        [Test]
        public async Task OnConnected_ClientWithConnectionIDConnected_AllClientsAreNotified()
        {
            // Arrange
            var clientConnectionWasBroadcast = false;
            dynamic all = new ExpandoObject();
            all.clientConnected = new Action<string>(connectionID => clientConnectionWasBroadcast = true);
            _mockClients.Setup(m => m.All)
                .Returns((ExpandoObject) all);

            // Act
            await _chatHub.OnConnected();

            // Assert
            Assert.That(clientConnectionWasBroadcast);
        }

        [Test]
        public async Task OnConnected_ClientWithConnectionIDConnected_UserHandlerRemembersConnectionID()
        {
            // Arrange
            dynamic all = new ExpandoObject();
            all.clientConnected = new Action<string>(connectionID => { });
            _mockClients.Setup(m => m.All)
                .Returns((ExpandoObject) all);

            // Act
            await _chatHub.OnConnected();

            // Assert
            Assert.That(UserHandler.ConnectedIds.Contains("JakeConnectionID"));
        }

        [Test]
        public async Task OnDisconnected_ClientWithConnectionIDDisconnected_AllClientsAreNotified()
        {
            // Arrange
            var clientDisconnectionWasBroadcast = false;
            dynamic all = new ExpandoObject();
            all.clientDisconnected = new Action<string>(connectionID => clientDisconnectionWasBroadcast = true);
            _mockClients.Setup(m => m.All)
                .Returns((ExpandoObject) all);

            // Act
            await _chatHub.OnDisconnected(true);

            // Assert
            Assert.That(clientDisconnectionWasBroadcast);
        }

        [Test]
        public async Task OnDisconnected_ClientWithConnectionIDDisconnected_UserHandlerForgetsConnectionID()
        {
            // Arrange
            dynamic all = new ExpandoObject();
            all.clientDisconnected = new Action<string>(connectionID => { });
            _mockClients.Setup(m => m.All)
                .Returns((ExpandoObject) all);

            // Act
            await _chatHub.OnDisconnected(true);

            // Assert
            Assert.That(UserHandler.ConnectedIds, Does.Not.Contain("JakeConnectionID"));
        }

        [Test]
        public async Task OnReconnected_ClientReconnected_CallerIsNotifiedOfReconnection()
        {
            // Arrange
            var callerWasNotifiedOfReconnection = false;
            dynamic caller = new ExpandoObject();
            caller.reconnected = new Action(() => callerWasNotifiedOfReconnection = true);
            _mockClients.Setup(m => m.Caller)
                .Returns((ExpandoObject) caller);

            // Act
            await _chatHub.OnReconnected();

            // Assert
            Assert.That(callerWasNotifiedOfReconnection);
        }

        [Test]
        public void Send_MessageReceived_MessageBroadcast()
        {
            // Arrange
            var broadcastMessageCalled = false;
            _chatRepositoryMock.Setup(repository => repository.Add(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(default(ChatMessage));
            dynamic all = new ExpandoObject();
            all.broadcastMessage = new Action<ChatMessage>(chatMessage => broadcastMessageCalled = true);
            _mockClients.Setup(m => m.All)
                .Returns((ExpandoObject) all);

            // Act
            _chatHub.Send("jake", "HELLO!");

            // Assert
            Assert.That(broadcastMessageCalled);
        }

        [Test]
        public void Send_ServerCommand_CallerReceivesServerCommandResponse()
        {
            // Arrange
            var serverCommandReceived = false;
            dynamic caller = new ExpandoObject();
            caller.serverCommandResponse = new Action<string>(message => serverCommandReceived = true);
            _mockClients.Setup(m => m.Caller)
                .Returns((ExpandoObject) caller);

            // Act
            _chatHub.Send("jake", "server status");

            // Assert
            Assert.That(serverCommandReceived);
        }
    }
}